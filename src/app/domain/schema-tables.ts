export class SchemaTables {
    tableCatalog: string;
    tableSchema: string;
    tableName: string;
    tableType: string;
    selfReferencingColumnName: string;
    referenceGeneration: string;
    userDefinedTypeCatalog: string;
    userDefinedTypeSchema: string;
    userDefinedTypeName: string;
    isInsertableInto: string;
    isTyped: string;
    commitAction: string;
}
