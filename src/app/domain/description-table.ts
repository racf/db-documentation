export class DescriptionTable {
    tableCatalog: String;
    tableSchema: String;
    tableName: String;
    columnName: String;
    ordinalPosition: String;
    columnDefault: String;
    isNullable: String;
    dataType: String;
    characterMaximumLength: String;
    dtdIdentifier: String;
    isSelfReferencing: String;
    isIdentity: String;
    identityGeneration: String;
    identityStart: String;
    identityIncrement: String;
    identityMaximum: String;
    identityMinimum: String;
    identityCycle: String;
    isGenerated: String;
    generationExpression: String;
}