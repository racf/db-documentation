import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { registerLocaleData } from '@angular/common';
import localeMx from '@angular/common/locales/es-MX';

/*
*Componentes
*/
import { AppComponent } from './app.component';
import { DrawerComponent } from './components/shared/drawer/drawer.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { UserComponent } from './components/private/user/user.component';
import { TablesComponent } from './components/private/tables/tables.component';
/*
*Rutas
*/
import { APP_ROUTING } from './app.routes';


/*
*Servicios
*/
import { SchemaTablesService } from './service/schema-tables.service';
import { DescriptionTableService } from './service/description-table.service';
registerLocaleData(localeMx);

@NgModule({
  declarations: [
    AppComponent,
    DrawerComponent,
    NavbarComponent,
    UserComponent,
    TablesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    APP_ROUTING
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-MX' },
    SchemaTablesService,
    DescriptionTableService
    //Servicios Generales
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
