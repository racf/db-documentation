import { Component, OnInit } from '@angular/core';
import { SchemaTablesService } from 'src/app/service/schema-tables.service';
import { SchemaTables } from 'src/app/domain/schema-tables';
import { Router } from '@angular/router';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.css']
})
export class DrawerComponent implements OnInit {
  schemasTables: SchemaTables[];
  constructor(private schemaTablesService: SchemaTablesService, private router: Router) { }

  ngOnInit() {
    this.getSchemaTables();
  }

  getSchemaTables(): void {
    this.schemaTablesService.getSchemaTables()
        .subscribe(response => {
          this.schemasTables = response;
          // this.router.navigate(['tables'], {queryParams: {table: encodeURIComponent(JSON.stringify(this.schemasTables[0]))}});
        });
  }

  infoSchemaTable(schemaTables: SchemaTables): void {
    this.router.navigate(['tables'], {queryParams: {table: encodeURIComponent(JSON.stringify(schemaTables))}});
  }


}
