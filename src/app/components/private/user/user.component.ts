import { Component, OnInit } from '@angular/core';
import { AuthenticationServiceService } from '../../../service/authentication-service.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [ AuthenticationServiceService ]
})
export class UserComponent implements OnInit {

  constructor(private authenticationServiceService: AuthenticationServiceService) { }

  ngOnInit() {
    // this.login();
  }

  /* login(): void {
    this.authenticationServiceService.login('admin@gmail.com', 'jwtpass')
        .subscribe(response => {
          console.log('RESP TOKEN ', response);
        });
  } */

}
