import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SchemaTablesService } from 'src/app/service/schema-tables.service';
import { SchemaTables } from 'src/app/domain/schema-tables';
import { DescriptionTable } from 'src/app/domain/description-table';
import { DescriptionTableService } from 'src/app/service/description-table.service';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {
  schemasTables: SchemaTables[];
  descriptionTables : DescriptionTable[];
  schemaTable: SchemaTables;
  constructor(private schemaTablesService: SchemaTablesService, private route: ActivatedRoute, private descriptionTableService: DescriptionTableService) {
  }

  ngOnInit() {
    // this.schemaTable = JSON.parse(decodeURIComponent(this.route.snapshot.queryParamMap.get('table')));
    this.route.queryParams.subscribe(queryParams => {
      // console.log("jajaja", JSON.parse(decodeURIComponent(queryParams['table'])));
      this.schemaTable = JSON.parse(decodeURIComponent(queryParams['table']));
      console.log('IMPRIME PARAMETRO', this.schemaTable);
      this.getDescriptionTable(this.schemaTable);
    });
  }

  getDescriptionTable(schemaTable: SchemaTables): void {
    this.descriptionTableService.getDescriptionTable(schemaTable.tableName, schemaTable.tableSchema)
      .subscribe(response => {
        this.descriptionTables = response;

        /* this.schemasTables = response; */
        console.log(this.descriptionTables);
        // this.router.navigate(['tables'], {queryParams: {table: encodeURIComponent(JSON.stringify(this.schemasTables[0]))}});
      });
  }

}
