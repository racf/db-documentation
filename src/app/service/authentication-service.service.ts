import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { url } from 'inspector';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationServiceService {
  static AUTH_TOKEN = 'http://localhost:8080/oauth/token';//'http://controlserverjwtid:XFyYDCYS0UT@localhost:8080/oauth/token';
  //'http://localhost:8080/oauth/token';
  // static TOKEN_AUTH_PASSWORD = 'XFyYDCYS0UT';
  // static TOKEN_AUTH_USERNAME = 'testjwtclientid';
  // static TOKEN_NAME = 'access_token';
  TOKEN_AUTH_PASSWORD = 'XFyYDCYS0UT';
  TOKEN_AUTH_USERNAME = 'controlserverjwtid';
  TOKEN_NAME = 'access_token';

  // clientId =  'controlserverjwtid';
  // clientSecret = 'XFyYDCYS0UT';
  // resourceIds = 'testjwtresourceid';

  constructor(private httpClient: HttpClient) {

  }

  login(username: string, password: string): Observable<any>  {
    const body = `username=admin@gmail.com&password=${encodeURIComponent(password)}&grant_type=password&client_secret=XFyYDCYS0UT&client_id=controlserverjwtid`;
    console.log("BODY: ", body);
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', 'Basic ' + btoa(this.TOKEN_AUTH_USERNAME + ':' + this.TOKEN_AUTH_PASSWORD));
    // const body = `username=${encodeURIComponent(username)}&password=${encodeURIComponent(password)}&grant_type=password`;
    // const headers = new HttpHeaders();
    // headers.append('Content-Type', 'application/x-www-form-urlencoded');
    // headers.append('Authorization', 'Basic ' + btoa(this.TOKEN_AUTH_USERNAME + ':' + this.TOKEN_AUTH_PASSWORD));

    /*return this.httpClient.post(AuthenticationServiceService.AUTH_TOKEN, body, {headers})
      .map(res => res.json())
      .map((res: any) => {
        if (res.access_token) {
          return res.access_token;
        }
        return null;
      });*/
      const options = {
        headers: new HttpHeaders().append('Content-Type', 'application/x-www-form-urlencoded')
        .append('Accept', 'application/json')
        .append('Authorization', 'Basic ' + btoa(this.TOKEN_AUTH_USERNAME + ':' + this.TOKEN_AUTH_PASSWORD))
        // params: new HttpParams().append('username', 'admin@gmail.com').append('password','jwtpass')
        // .append('grant_type','password')
      }
    return this.httpClient.post(AuthenticationServiceService.AUTH_TOKEN, body, {headers})
    .pipe(
      tap((result) => console.log('Resultado', result)),
      catchError(this.handleError<any>('getToken'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
