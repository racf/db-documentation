import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AppConstants } from '../app.constants';
import { Util } from '../utilities/util';

@Injectable({
  providedIn: 'root'
})
export class SchemaTablesService {
  private baseApiURL: string;
  constructor(private http: HttpClient) {
    this.baseApiURL = AppConstants.baseApiURL;
  }

  getSchemaTables(): Observable<any> {
    return this.http.get(this.baseApiURL.concat('/schemaTables'))
      .pipe(
        tap((result) => {}),
        catchError(Util.handleError<any>('getSchemaTables'))
      );
  }
}
