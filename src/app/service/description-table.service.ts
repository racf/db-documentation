import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AppConstants } from '../app.constants';
import { Util } from '../utilities/util';

@Injectable({
  providedIn: 'root'
})
export class DescriptionTableService {
  private baseApiURL: string;
  constructor(private http: HttpClient) {
    this.baseApiURL = AppConstants.baseApiURL;
    /* constructor() { } */
  }

  getDescriptionTable(tableName: string, tableSchema: string): Observable<any> {
    return this.http.get(this.baseApiURL.concat('/describeTable/').concat(tableName).concat('/').concat(tableSchema))
      .pipe(
        tap((result) => { }),
        catchError(Util.handleError<any>('getDescriptionTable'))
      );
  }
}
