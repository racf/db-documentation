export class MessageDTO {
    mac: string;
    type: string;
    request: string;
    code: number;
    content: object;
    constructor(mac: string, type: string, request: string, code: number, content: object) {
        this.mac = mac;
        this.type = type;
        this.request = request;
        this.code = code;
        this.content = content;
    }
}
