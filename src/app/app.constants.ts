export class AppConstants {
    public static get baseURL(): string { return 'http://192.168.1.95:8081'; }
    public static get baseApiURL(): string { return 'http://192.168.1.95:8081/api'; }
    public static get dateTimeFormat(): string { return 'yyyy-MM-dd'.concat('T').concat('HH:mm:ssZ'); }
    public static get localeDate(): string { return 'es-MX'; }
}
