//sudo npm install @types/stompjs
//sudo npm install @types/sockjs-client
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {AppConstants} from '../app.constants';
// import { DeviceComponent } from '../components/private/device/device.component';
// import { DevicesOnComponent } from '../components/private/device/devices-on/devices-on.component';

@Injectable({
    providedIn: 'root'
  })
export class WebSocketAPI {
    //Inicialización de variable
    baseURL: string;
    webSocketEndPoint: string;
    topic: string;
    stompClient: any;
    private subject = new Subject<any>();
    constructor() {
        // this.appComponent = appComponent;
        // this.devicesOnComponent = devicesOnComponent;
        this.baseURL = AppConstants.baseURL;
        this.webSocketEndPoint = this.baseURL.concat('/connect-websocket');
        this.topic = '/resp/server';
    }
    connect() {
        console.log('Initialize WebSocket Connection');
        const ws = new SockJS(this.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        const auxThis = this;
        auxThis.stompClient.connect({}, function(frame) {
            auxThis.stompClient.subscribe(auxThis.topic, function (sdkEvent) {
                auxThis.onMessageReceived(sdkEvent);
            });
            //_this.stompClient.reconnect_delay = 2000;
        }, this.errorCallBack);
    }

    disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log('Disconnected');
    }

    // on error, schedule a reconnection attempt
    errorCallBack(error) {
        console.log('errorCallBack -> ' + error);
        setTimeout(() => {
            this.connect();
        }, 5000);
    }

    /**
     * Send message to sever via web socket
     * @param {*} message
     */
    /*sendMessage(message: string) {
        console.log('calling logout api via web socket');
        this.stompClient.send('/app/message', {}, JSON.stringify(message));
    }

    onMessageReceived(message) {
        // console.log("Message Recieved from Server: " + message);
        //Acceder al body del mensaje que se recibe JSON.stringify(message.body)
        this.appComponent.handleMessage(message);
        this.devicesOnComponent.handleMessageDeviceOn(message);
    }*/

    sendMessage(message) {
        console.log('calling logout api via web socket', message);
        this.stompClient.send('/app/message', {}, JSON.stringify(message));
    }

    clearMessages() {
        this.subject.next();
    }
    onMessageReceived(message) {
        // this.subject.next({ text: message });
        console.log(message);
        this.subject.next(message);
    }
    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
