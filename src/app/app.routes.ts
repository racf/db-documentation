import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './components/private/user/user.component';
import { TablesComponent } from './components/private/tables/tables.component';

const APP_ROUTES: Routes = [
{ path: 'tables', component: TablesComponent },
{ path: 'users', component: UserComponent },
{ path: '', pathMatch: 'full', redirectTo: 'users'},
{ path: '**', pathMatch: 'full', redirectTo: 'users' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash: true});