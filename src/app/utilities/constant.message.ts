export class ConstantMessage {
    public static get successfulCreation(): string { return 'Creación exitosa...!'; }
    public static get successfulUpdate(): string { return 'Modificación exitosa...!'; }

}
