import { Observable, of, from } from 'rxjs';
import { AppConstants } from '../app.constants';
import { formatDate } from '@angular/common';
export class Util {
      /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  public static handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  public static dateFormat( object: object ) {
      let dateTimeFormat = null;
      let dateCreated = object['dateCreated'];
      let dateModified = object['dateModified'];
      console.log('DADADADATATATAT: ', dateCreated, dateModified);
      if ( dateCreated ) {
        if ( dateCreated !== null) {
          dateTimeFormat = formatDate(dateCreated, AppConstants.dateTimeFormat, AppConstants.localeDate);
          object['dateCreated'] = dateTimeFormat;
        }
      }
      if ( dateModified ) {
        if ( dateModified !== null) {
          dateTimeFormat = formatDate(dateModified, AppConstants.dateTimeFormat, AppConstants.localeDate);
          object['dateModified'] = dateTimeFormat;
        }
      }
      return object;
  }
}
